//
//  File.swift
//  Foursquare
//
//  Created by Jaroslav Chaninovicz on 22/05/15.
//  Copyright (c) 2015 Jaroslav Chaninovicz. All rights reserved.
//

import Foundation
import MapKit


class MapPlace: UIViewController, MKMapViewDelegate {
    let place : Place?
    let mapView = MKMapView()
    
    init(place: Place){
        self.place = place
        super.init(nibName: nil, bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMap()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.setNavigationBarHidden(false, animated: false)
        super.viewWillAppear(animated)
    }
    
    func setupMap() {
        mapView.mapType = .Standard
        mapView.frame = view.frame
        mapView.delegate = self
        view.addSubview(mapView)
        
        let latitude : Double? = place?.latitude?.doubleValue
        let longtitude : Double? = place?.longtitude?.doubleValue
        var location :CLLocationCoordinate2D?
        if (latitude != nil && longtitude != nil) {
                location = CLLocationCoordinate2D(
                latitude: latitude!,
                longitude: longtitude!
            )
        }
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location!, span: span)
        mapView.setRegion(region, animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location!
        annotation.title = place?.name
        annotation.subtitle = place?.address
        mapView.addAnnotation(annotation)
    }
}
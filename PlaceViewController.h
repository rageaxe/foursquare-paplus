//
//  Test.h
//  Foursquare
//
//  Created by Jaroslav Chaninovicz on 22/05/15.
//  Copyright (c) 2015 Jaroslav Chaninovicz. All rights reserved.
//

#ifndef Foursquare_Test_h
#define Foursquare_Test_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class Place;

@interface PlaceViewController : UIViewController
- (instancetype)initWithPlace:(Place *)place;
- (void)setupTips;
@property (strong, nonatomic) Place *place;
@end
#endif

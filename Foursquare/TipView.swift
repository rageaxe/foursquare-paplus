//
//  TipView.swift
//  Foursquare
//
//  Created by Jaroslav Chaninovicz on 23/05/15.
//  Copyright (c) 2015 Jaroslav Chaninovicz. All rights reserved.
//

import Foundation
import UIKit

class TipView : UIView {
    var tip : Tip
    var nameLastName : UILabel = UILabel()
    var imageView : UIImageView = UIImageView()
    var comment : UILabel = UILabel()
    
    init(tip : Tip) {
        self.tip = tip
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        self.backgroundColor = UIColor.redColor()
        setupTipView()
        tipLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupTipView() {
        nameLastName.textAlignment = NSTextAlignment.Center
        nameLastName.textColor = UIColor.whiteColor()
        
        if(tip.name == nil && tip.lastName == nil) {
            nameLastName.text = "Anonymous"
        } else if (tip.name != nil && tip.lastName != nil) {
            nameLastName.text = tip.name! + " " + tip.lastName!
        } else if (tip.name != nil) {
            nameLastName.text = tip.name!
        } else {
            nameLastName.text = tip.lastName!
        }
      
        nameLastName.font = UIFont.systemFontOfSize(18)
        
        comment.textAlignment = NSTextAlignment.Left
        comment.textColor = UIColor.whiteColor()
        comment.text = tip.comment
        comment.font = UIFont.systemFontOfSize(15)
        comment.numberOfLines = 4
        
        imageView.image = tip.avatar
    }
    
    func tipLayout() {
        imageView.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.addSubview(imageView)
        
        self.addConstraint(NSLayoutConstraint(item: imageView, attribute: .Left, relatedBy: .Equal, toItem: self, attribute: .Left, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: imageView, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 100))
        
        self.addConstraint(NSLayoutConstraint(item: imageView, attribute: .Top, relatedBy: .Equal, toItem: self, attribute: .Top, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: imageView, attribute: .Bottom, relatedBy: .Equal, toItem: self, attribute: .Bottom, multiplier: 1, constant: 0))
        
        
        nameLastName.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.addSubview(nameLastName)
        
        self.addConstraint(NSLayoutConstraint(item: nameLastName, attribute: .Left, relatedBy: .Equal, toItem: imageView, attribute: .Right, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: nameLastName, attribute: .Right, relatedBy: .Equal, toItem: self, attribute: .Right, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: nameLastName, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 30))
        self.addConstraint(NSLayoutConstraint(item: nameLastName, attribute: .Top, relatedBy: .Equal, toItem: self, attribute: .Top, multiplier: 1, constant: 0))
        
        comment.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.addSubview(comment)
        
        self.addConstraint(NSLayoutConstraint(item: comment, attribute: .Left, relatedBy: .Equal, toItem: imageView, attribute: .Right, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: comment, attribute: .Right, relatedBy: .Equal, toItem: self, attribute: .Right, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: comment, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 70))
        self.addConstraint(NSLayoutConstraint(item: comment, attribute: .Top, relatedBy: .Equal, toItem: nameLastName, attribute: .Bottom, multiplier: 1, constant: 0))
    }
}
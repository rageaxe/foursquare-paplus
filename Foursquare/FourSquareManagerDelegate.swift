//
//  FourSquareManagerDelegate.swift
//  Foursquare
//
//  Created by Jaroslav Chaninovicz on 22/05/15.
//  Copyright (c) 2015 Jaroslav Chaninovicz. All rights reserved.
//

import Foundation
protocol FourSquareManagerDelegate {
    func reloadTableView()
    func tipsLoaded()
}
//
//  FourSquareManager.swift
//  Foursquare
//
//  Created by Jaroslav Chaninovicz on 22/05/15.
//  Copyright (c) 2015 Jaroslav Chaninovicz. All rights reserved.
//

import Foundation


class FourSquareManager {
    private let clientID = "WQHJJKLLDMYOJGTXGPMQOTYKIN0FOL2GD0R3NXXBUJUDCZGD"
    private let clientSecret = "G2SRSOI1RZZYEOSKRLFZSDM4SMO4LQD2LVGTPC1AHTE3MKA3"
    private var places = [Place]()
    var delegate : FourSquareManagerDelegate?
    let session = NSURLSession.sharedSession()
    
    init(){
        
    }
    
    func loadData(){
        let url = "https://api.foursquare.com/v2/venues/explore?client_id=\(clientID)&client_secret=\(clientSecret)&v=20150520&near=lodz&section=food&limit=10&venuePhotos=1"
        let urlSTR : NSString = url.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let searchURL : NSURL = NSURL(string: urlSTR as String)!

       dispatch_async(dispatch_get_main_queue(), { () -> Void in
            var task = self.session.dataTaskWithURL(searchURL, completionHandler: {(data : NSData!, response : NSURLResponse!, error : NSError!) in
                if error == nil {
                    let jsonData  = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers, error: nil) as! NSDictionary
                    self.createPlacesArray(jsonData)
                }
            })
            task.resume()
        })
    }
    
    func createPlacesArray(places: NSDictionary) {
        for(var i = 0; i < places["response"]?["groups"]??[0]["items"]??.count; i++) {
            let photoPrefix = places["response"]?["groups"]??[0]["items"]??[i]["venue"]??["featuredPhotos"]??["items"]??[0]["prefix"] as? String
            let photoSuffix = places["response"]?["groups"]??[0]["items"]??[i]["venue"]??["featuredPhotos"]??["items"]??[0]["suffix"] as? String
            let photoUrl : String? = photoPrefix! + "300x300" + photoSuffix!
            let id = places["response"]?["groups"]??[0]["items"]??[i]["venue"]??["id"] as? String
            let name = places["response"]?["groups"]??[0]["items"]??[i]["venue"]??["name"] as? String
            let address = places["response"]?["groups"]??[0]["items"]??[i]["venue"]??["location"]??["address"] as? String
            let longtitude = places["response"]?["groups"]??[0]["items"]??[i]["venue"]??["location"]??["lng"] as?NSNumber
            let latitude = places["response"]?["groups"]??[0]["items"]??[i]["venue"]??["location"]??["lat"] as? NSNumber
            
            var place = Place(id: id, name: name, imageUrl: photoUrl)
            place.address = address
            place.longtitude = longtitude
            place.latitude = latitude
            self.places.append(place)
        }
        self.delegate?.reloadTableView()
    }
    
    func checkTips(place: Place) {
        let url = "https://api.foursquare.com/v2/venues/\(place.id!)/tips?client_id=\(clientID)&client_secret=\(clientSecret)&v=20150520"
        let urlSTR : NSString = url.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let searchURL : NSURL = NSURL(string: urlSTR as String)!
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            var task = self.session.dataTaskWithURL(searchURL, completionHandler: {(data : NSData!, response :  NSURLResponse!, error : NSError!) in
                if error == nil {
                    let jsonData  = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers, error: nil) as! NSDictionary
                    self.saveTipsToPlace(place, data: jsonData)
                }
            })
            task.resume()
        })
    }
    
    func saveTipsToPlace(place: Place, data: NSDictionary) {
        for(var i = 0; i < 3; i++) {
            let comment = data["response"]?["tips"]??["items"]??[i]["text"] as? String
            let firstName = data["response"]?["tips"]??["items"]??[i]["user"]??["firstName"] as? String
            let lastName = data["response"]?["tips"]??["items"]??[i]["user"]??["lastName"] as? String
            let prefix = data["response"]?["tips"]??["items"]??[i]["user"]??["photo"]??["prefix"] as? String
            let suffix = data["response"]?["tips"]??["items"]??[i]["user"]??["photo"]??["suffix"] as? String
            let photoUrl: String = prefix! + "100x100" + suffix!
            place.tips.append(Tip(name: firstName, lastName: lastName, imgUrl: photoUrl, comment: comment))
        }
        self.delegate?.tipsLoaded()
    }
    
    func getPlaces()->[Place] {
        return self.places
    }
}
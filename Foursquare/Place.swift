//
//  Place.swift
//  Foursquare
//
//  Created by Jaroslav Chaninovicz on 22/05/15.
//  Copyright (c) 2015 Jaroslav Chaninovicz. All rights reserved.
//
import Foundation
import UIKit

@objc class Place : NSObject {
    var name : String?
    var id : String?
    var address : String?
    var longtitude : NSNumber?
    var latitude : NSNumber?
    var image : UIImage?
    var tips = [Tip]()
    
    init(id: String?, name: String?, imageUrl: String?){
        self.id = id
        self.name = name
        if imageUrl != nil {
            let imgUrl : NSURL? = NSURL(string: imageUrl!)
            let data = NSData(contentsOfURL: imgUrl!)
            self.image = UIImage(data: data!)
        }
    }
}
//
//  TipsViewController.swift
//  Foursquare
//
//  Created by Jaroslav Chaninovicz on 23/05/15.
//  Copyright (c) 2015 Jaroslav Chaninovicz. All rights reserved.
//

import Foundation
import UIKit

@objc class TipsViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.grayColor()
    }
    
    func setupTips(tips: [Tip]) {
        var lastView :TipView?
        
        for(var i = 0; i < tips.count; i++) {
            var tipView : TipView = TipView(tip: tips[i])
            tipView.setTranslatesAutoresizingMaskIntoConstraints(false)
            self.view.addSubview(tipView)
            self.view.addConstraint(NSLayoutConstraint(item: tipView, attribute: .Left, relatedBy: .Equal, toItem: self.view, attribute: .Left, multiplier: 1, constant: 0))
            
            self.view.addConstraint(NSLayoutConstraint(item: tipView, attribute: .Right, relatedBy: .Equal, toItem: self.view, attribute: .Right, multiplier: 1, constant: 0))
            
            self.view.addConstraint(NSLayoutConstraint(item: tipView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 100))
            
            if(lastView == nil) {
                self.view.addConstraint(NSLayoutConstraint(item: tipView, attribute: .Top, relatedBy: .Equal, toItem: self.view, attribute: .Top, multiplier: 1, constant: 0))
            } else {
                self.view.addConstraint(NSLayoutConstraint(item: tipView, attribute: .Top, relatedBy: .Equal, toItem: lastView, attribute: .Bottom, multiplier: 1, constant: 5))
            }
            lastView = tipView
        }
    }
    
}
//
//  Test.m
//  Foursquare
//
//  Created by Jaroslav Chaninovicz on 22/05/15.
//  Copyright (c) 2015 Jaroslav Chaninovicz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlaceViewController.h"
#import "Foursquare-Swift.h"

@interface PlaceViewController()
@property (strong, nonatomic) MapPlace *map;
@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UILabel *addressLabel;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIButton *mapButton;
@property (strong, nonatomic) TipsViewController *tipsView;
@end

@implementation PlaceViewController

- (instancetype)initWithPlace:(Place *)place{
    self = [super init];
    if (self) {
        self.place = place;
        [self setupPlaceView];
    }
    return self;
}

- (void) viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor brownColor];
    [self placeViewLayout];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setHidesBarsOnSwipe:true];
    [super viewWillAppear:animated];
}

- (void)setupTips {
    [self.tipsView setupTips:self.place.tips];
}

- (void)setupPlaceView {
    _tipsView = [[TipsViewController alloc] init];
    _map = [[MapPlace alloc] initWithPlace:_place];
    _mapButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [_mapButton addTarget:self action:@selector(showMap:) forControlEvents:UIControlEventTouchUpInside];
    [_mapButton setTitle:@"Show Map" forState:UIControlStateNormal];
    
    _nameLabel = [[UILabel alloc] init];
    _nameLabel.textAlignment = NSTextAlignmentCenter;
    _nameLabel.textColor = [UIColor whiteColor];
    _nameLabel.text = _place.name;
    _nameLabel.font = [UIFont systemFontOfSize:20.f];
    
    _addressLabel = [[UILabel alloc] init];
    _addressLabel.textAlignment = NSTextAlignmentCenter;
    _addressLabel.textColor = [UIColor whiteColor];
    _addressLabel.text = _place.address;
    _addressLabel.font = [UIFont systemFontOfSize:20.f];
    
    _imageView = [[UIImageView alloc] init];
    _imageView.image = _place.image;
}

- (void) showMap:(UIButton*)sender {
    [self.navigationController pushViewController:_map animated:true];
}

- (void)placeViewLayout {
    [self.view addSubview:_nameLabel];
    [_nameLabel setTranslatesAutoresizingMaskIntoConstraints:false];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:_nameLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:30.0f];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:_nameLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:20.0f];
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:_nameLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0f constant:0.0f];
    NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem:_nameLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0f constant:0.0f];
    [self.view addConstraint:height];
    [self.view addConstraint:top];
    [self.view addConstraint:left];
    [self.view addConstraint:right];
    
    
    [self.view addSubview:_imageView];
    [_imageView setTranslatesAutoresizingMaskIntoConstraints:false];

    NSLayoutConstraint *imageViewTop = [NSLayoutConstraint constraintWithItem:_imageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_nameLabel attribute:NSLayoutAttributeBottom multiplier:1.0f constant:20.0f];
    
    NSLayoutConstraint *imageViewCenter = [NSLayoutConstraint constraintWithItem:_imageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f];
    
    NSLayoutConstraint *imageViewHeight = [NSLayoutConstraint constraintWithItem:_imageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:200.0f];
    
    NSLayoutConstraint *imageViewWidth = [NSLayoutConstraint constraintWithItem:_imageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:300.0f];
    
    [self.view addConstraint:imageViewTop];
    [self.view addConstraint:imageViewCenter];
    [self.view addConstraint:imageViewHeight];
    [self.view addConstraint:imageViewWidth];
    
    
    [self.view addSubview:_addressLabel];
    [_addressLabel setTranslatesAutoresizingMaskIntoConstraints:false];
    NSLayoutConstraint *addressHeight = [NSLayoutConstraint constraintWithItem:_addressLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:30.0f];
    NSLayoutConstraint *addressBottom = [NSLayoutConstraint constraintWithItem:_addressLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0f constant:-40.0f];
    NSLayoutConstraint *addressLeft = [NSLayoutConstraint constraintWithItem:_addressLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0f constant:0.0f];
    NSLayoutConstraint *addressright = [NSLayoutConstraint constraintWithItem:_addressLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0f constant:0.0f];
    [self.view addConstraint:addressHeight];
    [self.view addConstraint:addressBottom];
    [self.view addConstraint:addressLeft];
    [self.view addConstraint:addressright];
    
    _mapButton.frame = CGRectMake(100.0, 630.0, 160.0, 40.0);
    [self.view addSubview:_mapButton];
    
    
    [_tipsView.view setTranslatesAutoresizingMaskIntoConstraints:false];
    [self.view addSubview:_tipsView.view];
    NSLayoutConstraint *placeViewControllerTop = [NSLayoutConstraint constraintWithItem:_tipsView.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_imageView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.0f];
    NSLayoutConstraint *placeViewControllerLeft = [NSLayoutConstraint constraintWithItem:_tipsView.view attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0f constant:0.0f];
    NSLayoutConstraint *placeViewControllerRight = [NSLayoutConstraint constraintWithItem:_tipsView.view attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0f constant:0.0f];
    NSLayoutConstraint *placeViewControllerHeight = [NSLayoutConstraint constraintWithItem:_tipsView.view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:300.0f];
    [self.view addConstraint:placeViewControllerTop];
    [self.view addConstraint:placeViewControllerLeft];
    [self.view addConstraint:placeViewControllerRight];
    [self.view addConstraint:placeViewControllerHeight];
}

@end
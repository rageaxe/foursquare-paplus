

import Foundation
import UIKit

class ViewController: UITableViewController, UITableViewDataSource, UITableViewDelegate, FourSquareManagerDelegate {
    let textCellIdentifier = "PlacesCell"
    let fourSquareManager : FourSquareManager = FourSquareManager()
    var placeViews = [PlaceViewController]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: textCellIdentifier)
        self.tableView.rowHeight = 100
        fourSquareManager.delegate = self
        fourSquareManager.loadData()
    }
    
    func tipsLoaded() {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            for place : PlaceViewController in self.placeViews {
                if(place.place.tips.count != 0) {
                    place.setupTips()
                }
            }
        })
    }
    func reloadTableView() {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            for place in self.fourSquareManager.getPlaces() {
                self.fourSquareManager.checkTips(place)
                self.placeViews.append(PlaceViewController(place: place))
            }
            self.tableView.reloadData()
        })
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController?.hidesBarsOnSwipe = false
        super.viewWillAppear(animated)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fourSquareManager.getPlaces().count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(textCellIdentifier, forIndexPath: indexPath) as! UITableViewCell
        
        let row = indexPath.row
        cell.textLabel?.text = fourSquareManager.getPlaces()[row].name
        
        if fourSquareManager.getPlaces()[row].image != nil {
            cell.imageView?.image = fourSquareManager.getPlaces()[row].image
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let row = indexPath.row
        navigationController?.pushViewController(placeViews[row], animated: true)
    }
}
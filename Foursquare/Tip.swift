//
//  Tip.swift
//  Foursquare
//
//  Created by Jaroslav Chaninovicz on 23/05/15.
//  Copyright (c) 2015 Jaroslav Chaninovicz. All rights reserved.
//

import Foundation
import UIKit

class Tip {
    
    var name : String?
    var lastName: String?
    var avatar : UIImage?
    var comment : String?
    
    init(name: String?, lastName: String?, imgUrl: String?, comment: String?){
        self.name = name
        self.lastName = lastName
        self.comment = comment
        
        if imgUrl != nil {
            let imgNSURL : NSURL? = NSURL(string: imgUrl!)
            let data = NSData(contentsOfURL: imgNSURL!)
            self.avatar = UIImage(data: data!)
        }
    }
}